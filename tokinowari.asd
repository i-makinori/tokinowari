
(in-package :cl-user)

(defpackage tokinowari-asd
  (:use :cl :asdf))

(in-package :tokinowari-asd)


(defsystem tokinowari
  :author "Makinori Ikegami"
  :description "tokinowari.* portal page"
  :version "fai"
  :license "MIT"
  :depends-on (:hunchentoot
               :cl-fad
               :inferior-shell
               :cl-yaml
               :cl-markup)
  :components ((:file "package")
               (:module "src"
                :components
                ((:file "server")
                 (:file "template")
                 (:file "route")
                 (:file "test")
                 ))))

               
