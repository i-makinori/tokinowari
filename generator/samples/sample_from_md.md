---
title: マークダウンと変換に関する試験
author: Makinori Ikegami.
description: |
    this is a long
    description
    
---


# Pandocによるmarkdownからhtmlへの変換の試験

### はじめに

esse胞体の恣意観測曲線

静的構造化Webコンテンツの作成にあたって、其々のページとなる静的ファイルと共通のテンプレートを変換プログラムに通すことで、構造を吸収しつつ、それぞれページについては体裁が統一された存在物を生成するようにした。




### 見出し 

# h1
## h2
### h3
#### h4
##### h5
###### h6


### 引用

> 複数行に渡って引用する。
> blockquote
> > on line's head to quote it's line.

### 文字装飾

段落は空白の行ごとに区切られる。

*斜体*, **太字**, そして `行内でのコード`。


### リスト

リストは、

- ひとつめ
- ふたつめ
- みっつめ

又は、

- a
- b
  - b1 on b
  - b1 on b
- f
- g

更に、

+ あ
+ い
+ う


数値付きの配列

1. 1です
1. 2です
1. 3です
0. 4です
8. 5です. 入れ子にもできます



### 水平線

水平線 `<br />`

---

***

### リンク

[google](https://www.google.com) とか、
[yahoo](https://www.google.com) とか。

画像。


![ドメイン内部にある画像](samples/lambda.png)
![外部リンクの先にある(かも知れない)画像](https://www.photock.jp/photo/middle/photo0000-0705.jpg)


### エスケープ

地の文で表現に用いた文字が、markdown言語の構文の文字として解釈されることで、望まれない文書の構造の崩壊が発生しうるが、
これを防ぐ為に、地の文を記述する際にエスケープ文字を衝突しうる文字に付与する。

\\ \` \* \_ \{ \} \[ \] \( \) \# \+ \- \. \!





### ソースコード

- てぃげすばっ

```
すぱげってぃ
ぱげすってぃ
すげぱってぃ
っげぱすてぃ
てげぱすっぃ
```

- haskell

``` {.haskell}
functor :: forall m a b c.
           ( Functor m
           , Arbitrary b, Arbitrary c
           , CoArbitrary a, CoArbitrary b
           , Show (m a), Arbitrary (m a), EqProp (m a), EqProp (m c)) =>
           m (a,b,c) -> TestBatch
functor = const ( "functor"
                , [ ("identity", property identityP)
                , ("compose" , property composeP) ]
                )
 where
   identityP :: Property
   composeP  :: (b -> c) -> (a -> b) -> Property

   identityP = fmap id =-= (id :: m a -> m a)
   composeP g f = fmap g . fmap f =-= (fmap (g.f) :: m a -> m c)
```

### 数式

MathJaxを用いた。

$x$に関する二次方程式は $ax^2+bx+c=0$ の形に表され、解くと、
$$x = \frac{-b\pm\sqrt{b^2-4ac}}{2a} $$となる。

