---
title: Notebind list 
---

# Notebinds of /notebinds/

<div class="notebind-item">
  <div style="text-align:left;">/notebinds/sample_from_md.html
    <span style="float:right;"><a href="sample_from_md.md">Markdown</a></span>
  </div>
  <a class="link" href="/notebinds/sample_from_md.html">マークダウンと変換に関する試験</a>
  <br />
  this is sample file written in Markdown to apply conversion scripts. <br />
this (markdown) file contains many of tags and syntax of pure Markdown an .........
  <br />
  <div class="metatexts">作成日時: 2021年08月08日 00時54分44秒 G ...最終更新: 2021年08月08日 23時53分18秒 G</div>
</div>
<hr />

<div class="notebind-item">
  <div style="text-align:left;">/notebinds/why-portal-page.html
    <span style="float:right;"><a href="why-portal-page.md">Markdown</a></span>
  </div>
  <a class="link" href="/notebinds/why-portal-page.html">"自己ポータルサイトの開設に向けて。"</a>
  <br />
  World Wide Web上への自己ページは何度か作ってきかたものだが、
作る度に管理が遠のいてゆき、毎度のこと、閉鎖してきてしまった、、。 <br />
現実に対するネットの、自分の精神的な位置付けを基に、自分が自然だと思うWebページについて妄想し、
続くWebシステムが、どのような仕組みで .........
  <br />
  <div class="metatexts">作成日時: 2021年08月06日 12時29分41秒 G ...最終更新: 2021年08月09日 20時05分32秒 M</div>
</div>
<hr />

<div class="notebind-item">
  <div style="text-align:left;">/notebinds/markdown-sample.html
    <span style="float:right;"><a href="markdown-sample.md">Markdown</a></span>
  </div>
  <a class="link" href="/notebinds/markdown-sample.html">" pandocによるmarkdownからhtmlへの変換の試験"</a>
  <br />
   .........
  <br />
  <div class="metatexts">作成日時: 2021年08月06日 12時29分41秒 G ...最終更新: 2021年08月09日 15時50分34秒 M</div>
</div>
<hr />


# List Directory of /notebinds/

- [/notebinds/sample_from_md.md](/notebinds/sample_from_md.md)
- [/notebinds/index.html](/notebinds/index.html)
- [/notebinds/test.txt](/notebinds/test.txt)
- [/notebinds/markdown-sample.html](/notebinds/markdown-sample.html)
- [/notebinds/sample_from_md.html](/notebinds/sample_from_md.html)
- [/notebinds/why-portal-page.html](/notebinds/why-portal-page.html)
- [/notebinds/why-portal-page.md](/notebinds/why-portal-page.md)
- [/notebinds/index.md](/notebinds/index.md)
- [/notebinds/lambda.png](/notebinds/lambda.png)
- [/notebinds/markdown-sample.md](/notebinds/markdown-sample.md)
