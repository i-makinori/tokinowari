---
title: " pandocによるmarkdownからhtmlへの変換の試験"

---



## テスト

### 修飾文字

*斜体*, **太字**, そして `行内でのコード`。

### 見出し 

# h1

## h2

### h3

#### h4

##### h5

###### h6

### 複数行の引用

複数行に渡って背景たるdivを適用する。


> blockquote
> 引用2


### リスト

配列は、かの如く:

- ひとつめ
- ふたつめ
- みっつめ

又は、

- a
- b
  - b1 on b
  - b1 on b
- f
- g

更に、

+ あ
+ い
+ う


数値付きの配列

1. 1です
2. 2です
5. 3です
0. 4です
8. 5です. 入れ子にもできます


### ソースコード

```
ソースコードの節の中身は、そのまま表示されます。

改行もされます。

```

```python
name="language's code are highlighted"
```


### 水平線

水平線 `<br />`

---
 
***

### リンク

[google](https://www.google.com) とか、
[yahoo](https://www.google.com) とか。

画像。

![altenative text. himejuon-image. but 404](https://www.pakutaso.com/shared/img/thumb/mitte820G_TP_V.jpg)

### エスケープ

地の文で表現に用いた文字が、markdown言語の構文の文字として解釈されることで、望まれない文書の構造の崩壊が発生しうるが、
これを防ぐ為に、地の文を記述する際にエスケープ文字を衝突しうる文字に付与する。

\\ \` \* \_ \{ \} \[ \] \( \) \# \+ \- \. \!
