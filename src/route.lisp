
(in-package :tokinowari)

;;; route

(defun default-tokinowari-table ()
  (nconc
   (list
    (hunchentoot:create-static-file-dispatcher-and-handler
     "/favicon.ico"
     (make-pathname :name "favicon-beta" :type "ico" :version nil
                    :defaults *static-file-directory-pathname*))
    (hunchentoot:create-folder-dispatcher-and-handler
     "/statics/"
     (make-pathname :name nil :type nil :version nil
                    :defaults *static-file-directory-pathname*))
    ;;(create-sphere-dispatcher-and-handler
    ;;"/written-sphere/" *kura-directory-pathname*)
    ;; (create-librarian-edit-writing-dispatcher-and-handler
    ;;"/librarian/edit-writing/" *kura-directory-pathname*))
    ;;(hunchentoot:create-folder-dispatcher-and-handler
    ;;"/media-spheres/"
    ;;(make-pathname :name nil :type nil :version nil
    ;;:defaults *media-sphere-directory*))
    )
   (mapcar
    (lambda (args)
      (apply 'hunchentoot:create-prefix-dispatcher args))
    '()
  )))

(defmacro define-handler (description lambda-list &body body)
    `(hunchentoot:define-easy-handler ,description ,lambda-list ,@body))


;;; pages

;; index

(define-handler (indexr :uri "/") () 
  (tokinowari-template 
   :title "Portal"
   :contents 
   (markup
    (:p "Wellocome"))))

(define-handler (index :uri "") ()
  (setf (hunchentoot:content-type*) "text/html")
  (hunchentoot::redirect "/"))


;; esse

(defun page-list-of-esse ()
  (tokinowari-template
   :title "存在物総覧"
   :contents
   (html-of-list-tree (tree-whole-esse-directory)))
  )


(defun html-of-list-tree (tree)
  (let ((markup:*auto-escape* nil)
)
        )
    (cond ((consp tree)
           (markup:markup
            (:ul
             (:li (:b (write-to-string (car tree))))
             (loop for leaf in (cdr tree)
                   collect (markup:markup (:li (html-of-list-tree leaf)))))))
          (tree (write-to-string tree))
          ((null tree) nil)))


(defun tree-whole-directories-aux (directory)
  (cons directory
        (mapcar 
         #'(lambda (path)
             (cond ((cl-fad:directory-exists-p path) (tree-whole-directories-aux path))
                   ((cl-fad:file-exists-p path) path)
                   (t nil)))
         (cl-fad:list-directory directory))))

(defun tree-whole-esse-directory ()
  (tree-whole-directories-aux *esse-file-directory-pathname*))


(define-handler (index :uri "/esse") ()
  (setf (hunchentoot:content-type*) "text/html")
  (page-list-of-esse)
  )


;; errors

(defun page-404 (&optional (message "404 - not found"))
  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)  
  (tokinowari-template
   :title message
   :contents
   (markup
    (:div :class "article"
     (:p
      (:a :href (hunchentoot:request-uri*) (hunchentoot:request-uri*))
      " is not defined.")))))

(defmethod hunchentoot:acceptor-status-message
    (acceptor (http-status-code (eql 404)) &key)
  (page-404))


