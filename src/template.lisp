(in-package :tokinowari)

;;; template

(defun tokinowari-template-header (title)  
  (markup
   (:head
    (:title (format nil "~A~A" title "- n次元情報空間因果律基底研究室 -"))
    (:link :rel "stylesheet" :href "/statics/css/reset.css") 
    (:link :rel "stylesheet" :href "/statics/css/latex-css/style.css") 
    (:link :rel "stylesheet" :href "/statics/css/latex-css/lang/ja.css") 
    (:link :rel "stylesheet" :href "/statics/css/composition.css") 
    )))
  

(defun tokinowari-template (&key (title "") (contents ""))
  (xhtml
   :lang "ja"
   (raw (tokinowari-template-header title))
   (:body

    ;; header
    (:p
     " | " (:a :href "/" "n次元情報空間因果律基底研究室")
     " | " (:a :href "/esse" "雑文")
     ;; " | " (:a :href "/board" "発狂場")
     ;; " | " (:a :href "/renewal-logs" "更新記録")
     ;;" | " (:a :href "/eng/" "English")
     " | "
     (:hr))
    
    ;; contents    
    (:h1 title)
    (raw contents)

    ;; footer
    (:hr)
    (:center (:p "池上 蒔典 (Makinori Ikegami) <maau3p@gmail.com>"
              ;; "| (管理室)")
              ))
    )))
