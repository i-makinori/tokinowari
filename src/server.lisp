(in-package :tokinowari)


;;; config

(defparameter *system-root-directory-pathname*
  (cl-fad:pathname-directory-pathname
   ;;(load-time-value
   ;;(or *compile-file-pathname* *load-pathname*))))
   ;;#P"~/Documents/tokinowari/"
   (uiop:getcwd)))

(defvar *static-file-directory-pathname*
  ;; modus is common form of domain, which is static.
  (cl-fad:merge-pathnames-as-directory *system-root-directory-pathname*
                                       #P"statics/"))

(defvar *esse-file-directory-pathname*
  (cl-fad:merge-pathnames-as-directory *system-root-directory-pathname*
                                       #P"esse/"))



;;; server

(defvar *acceptor* nil)

(defun stop-server ()
  (when (and *acceptor* 
             (hunchentoot::started-p *acceptor*))
    (hunchentoot:stop *acceptor*)))

(defun start-server (&key (port 4242))
  "application server starting"
  (stop-server)
  (setq hunchentoot:*dispatch-table* nil)
  (hunchentoot:start
   (setf *acceptor* (make-instance 'hunchentoot:easy-acceptor :port port)))
  ; (asdf:oos 'asdf:load-op :hunchentoot-test)
  (setq hunchentoot:*dispatch-table*
        (append hunchentoot:*dispatch-table*
                (default-tokinowari-table)))
  ;; update json kura-base
  ;;(reload-whole-spheres-states!)
  )


