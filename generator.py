import os
import sys

# configs

this_script_file_directory = os.path.dirname(__file__)

def xxx_path(relative):
    return os.path.join(this_script_file_directory, relative, "");

esse_directory = xxx_path("esse/")
notebinds_directory = xxx_path("esse/notebinds")
public_directory = xxx_path("public/")
statics_directory = xxx_path("esse/statics/")
generator_directory = xxx_path("generator/")


# utils

def call_shell(command, echo_command=None):
    if (echo_command):
        print(command)
    os.system(command)

def get_extension(pathname):
    p, ext = os.path.splitext(pathname)
    return ext

def cp_file (source, to, stream=None):
    to_dirname = os.path.dirname(to)
    call_shell("mkdir -p %s" % to_dirname,
               echo_command=True)
    call_shell("cp --update {} {}".format(source, to),
               echo_command=True)

def copy_file_if_updated (source_file, source_directory, target_directory):
    source_relative = os.path.relpath(source_file, source_directory)
    to = os.path.join(target_directory, source_relative)
    #to_dirname = os.path.dirname(to)
    if not(os.path.isfile(to)) or os.path.getmtime(source_file) > os.path.getmtime(to):
        cp_file(source_file, to,stream=True)
        return (source_file, to)
    return None


def list_whole_directory(stack, memos, removes=[]):
    if stack == []:
        return memos;

    path=stack[0]
    stackd = stack[1:]
    if os.path.basename(path) in removes:
        pass
    elif os.path.isdir(path):
        memos = memos+[path]
        stackd.extend(list(map(lambda f: os.path.abspath(os.path.join(path, f)),
                               os.listdir(path))))
        stackd=stackd
    elif os.path.isfile(path):
        memos = memos+[path]
    else:
        raise;
    
    return list_whole_directory(stackd, memos, removes=removes)


def copy_updated_files_whole_directory (source_directory, target_directory, ignores):
    source_directory_paths = list_whole_directory([source_directory], [],
                                                  removes=ignores)
    updated_files = []
    for source_path in source_directory_paths:
        if os.path.isfile(source_path):
            is_updated = copy_file_if_updated(source_path, source_directory, target_directory)
            if (is_updated) : updated_files += [is_updated]
    return updated_files


# Esse

'''
class Esse:
    'Esse class treats path between source and public.'
    source_abs_path = ''
    public_abs_path = ''
    source_relative = ''
    cpmpiled_tml = None
    cpmpiled_pdf = None
    
    def __init__(self, source_abs_path, source_root):
        self.source_abs_path = source_abs_path
        self.source_relative = os.path.relpath(source_abs_path, source_root)
        self.public_abs_path = os.path.join(public_directory, self.source_relative)

    def print(self):
        print("gen:", self.source_abs_path)
        print("rel:", self.source_relative)
        print("pub:", self.public_abs_path)
'''

# apply converter to updated markdown

def apply_md_to_tml(md_file):

    cwd=os.getcwd()
    
    call_shell("cd %s" % generator_directory,
               echo_command=True)
    os.chdir(generator_directory)
    
    # make md_to_tml MD_FILE=samples/sample_from_md.md 
    call_shell("make md_to_tml MD_FILE=%s" % md_file,
               echo_command=True)
    
    os.system("cd %s" % this_script_file_directory)
    os.chdir(cwd)
    
    return md_file


def apply_md_to_tml_for_updated_esse_files (updated_files):
    paths = [path for path in updated_files
             if get_extension(path[0]) == ".md"]

    for path in paths:
        copied_md_file = path[1]
        apply_md_to_tml(copied_md_file)
    
    os.system("cd %s" % this_script_file_directory)
    os.chdir(this_script_file_directory)        
    return paths


# generate index

import markdown
import pathlib
import time

def time_string(localtime):
    return time.strftime('%Y年%m月%d日 %H時%M分%S秒', localtime)

# list directory

def lsdir_item (path):
    template = '- [{url}]({url})\n'
    url = "/" + os.path.relpath(path, public_directory)
    return template.format(url=url)


def list_directory_md_str (directory):
    paths = [os.path.join(directory, path) for path in  os.listdir(directory)]
    mdstr = ""
    
    for path in paths:
        mdstr += lsdir_item(path)

    return mdstr


# notebind list

item_template='''
<div class="notebind-item">
  <div style="text-align:left;">{url}
    <span style="float:right;"><a href="{md_url}">Markdown</a></span>
  </div>
  <a class="link" href="{url}">{title}</a>
  <br />
  {early_text} .........
  <br />
  <div class="metatexts">作成日時: {created} ...最終更新: {modified}</div>
</div>
<hr />
'''


# Modified date

def public_file_path_to_esse_file_path (public_path):
    maybe_path = os.path.join(esse_directory,
                              os.path.relpath(public_path, public_directory))
    return maybe_path if os.path.exists(maybe_path) else None


import re

def modified_dates_file (path):
    "timestump form "

    command = '''git log --pretty="format:%ci" {path}'''.format(path=path)
    git_lastmod_maybe = os.popen(command).read()

    date_type = re.compile(r"""()""",re.VERBOSE)
    modified_list= []

    for line in git_lastmod_maybe.splitlines():
        m = re.match(r'^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}) (\S{5})$'
                 , line, re.I)

        if m is None or len(m.group(7)) > 5:
            # datestr = 'File ' + time_string(time.localtime(os.path.getmtime(path)))
            pass
        else:
            datestr = '%s年%s月%s日 %s時%s分%s秒 G' % (m.group(1), m.group(2), m.group(3),
                                                        m.group(4), m.group(5), m.group(6))
            modified_list += [datestr]

    modified_list += [time_string(time.localtime(os.path.getmtime(path))) + ' M']
    modified_list += [time_string(time.localtime(os.path.getctime(path))) + ' C']
    
    modified_list.sort()
    
    return modified_list


def notebind_item (path):
    directory = os.path.dirname(path)
    # md
    md = markdown.Markdown(extensions = ["meta"])
    md_txt = pathlib.Path(path).read_text(encoding="utf-8")
    md.convert(md_txt)
    # print(md.Meta)
    
    # early_text
    early_text = ""
    if 'abstract' in md.Meta:
        if_bar = 1 if md.Meta['abstract'][0] == "|" else 0
        early_text='\n'.join(md.Meta['abstract'][if_bar:])
    early_text += ""
    early_text = early_text[:149]
    #print(early_text)
    
    # timestump
    modified_times = modified_dates_file(public_file_path_to_esse_file_path(path))
    #time_modified = time_string(time.localtime(os.path.getmtime(path)))
    time_modified = modified_times[-1]
    # time_created = time_string(time.localtime(os.path.getctime(path)))
    time_created = modified_times[0]
    
    # url
    url = "/" + os.path.relpath(os.path.join(os.path.dirname(path),
                                             os.path.splitext(path)[0]) + ".html",
                                public_directory)
    md_url = os.path.relpath(path, directory)

    # title
    title = md.Meta['title'][0] if 'title' in md.Meta else os.path.basename(url)

    return item_template.format(title=title, url=url, md_url=md_url, early_text=early_text,
                                modified=time_modified, created=time_created)


def notebind_list_page (directory):
    # notelist_tml_path = os.path.join(directory, "_notelist.html")
    # notebind_list_md_path = os.path.join(directory, "_notebind_list.md")
    notebind_list_md_path = os.path.join(directory, "index.md")

    paths = [os.path.join(directory, path) for path in  os.listdir(directory)
             if get_extension(path) == ".md" and os.path.basename(path) != "index.md"]

    mdstr = "---\n"
    mdstr += "title: Notebind list \n" #%
    mdstr += "---\n\n# Notebinds of /%s/\n" %  os.path.relpath(directory, public_directory)
    
    
    for path in paths:
        mdstr += notebind_item(path)
        
    #print(mdstr)
    #print(index_tml_path)

    mdstr += "\n\n# List Directory of /%s/\n\n" %  os.path.relpath(directory, public_directory)
    mdstr += list_directory_md_str(directory)
    
    index_md = pathlib.Path(notebind_list_md_path)
    index_md.write_text(data=mdstr, encoding='utf-8')
    
    apply_md_to_tml(notebind_list_md_path)
    
    pass



def auto_index (path_list):
    pass



# update whole directory

removes = []

if __name__ == "__main__":
    
    # 1. move changed files
    # 1. convert documents
    # 1. clean not needed

    updated = []
    # move whole updated files in statics/* to public/statics/
    # updated.extend(copy_updated_files_whole_directory
    # (statics_directory, os.path.join(public_directory, "statics"),
    # [".git", ".github"]))
    
    # move whole updated files in esse/ to public/
    updated.extend(copy_updated_files_whole_directory
                   (esse_directory, public_directory,
                    [".git", ".github"]))

    updated=updated
    print(updated)

    # move favicon.ico
    copy_file_if_updated(os.path.join(this_script_file_directory,"esse/statics/favicon.ico"),
                         statics_directory,
                         public_directory)
    
    # map(convert , remove-if(not types of md))
    updated_markdown = apply_md_to_tml_for_updated_esse_files(updated)
    print(updated_markdown)
    
    # map(convert , remove-if(not types of tex))
    # implement here


    # make notebind list
    if updated != [] or True:
        notebind_list_page(os.path.join(public_directory, "notebinds/"))

    # call(rm)
    
    exit();


def test_directory():
    # print(list(os.listdir(esse_directory)))
    ## tree ,absolute, relative
    #ta_list= list_whole_directory([esse_directory], [])
    #tr_list= list(map(lambda ap: os.path.relpath(ap, esse_directory),
    #ta_list))
    #print("memos:", tr_list)
    pass
